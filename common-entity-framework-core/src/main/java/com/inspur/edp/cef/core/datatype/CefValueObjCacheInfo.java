/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.datatype;

import com.inspur.edp.cef.spi.determination.IValueObjRTDtmAssembler;
import com.inspur.edp.cef.spi.entity.info.CefDataTypeInfo;
import com.inspur.edp.cef.spi.validation.IValueObjRTValidationAssembler;

public class CefValueObjCacheInfo extends CefDataTypeCacheInfo {
private IValueObjRTValidationAssembler afterModifyValsAssembler;

  public IValueObjRTValidationAssembler getAfterModifyValsAssembler() {
    return afterModifyValsAssembler;
  }

  public void setAfterModifyValsAssembler(
      IValueObjRTValidationAssembler afterModifyValsAssembler) {
    this.afterModifyValsAssembler = afterModifyValsAssembler;
  }

  private IValueObjRTValidationAssembler beforeSaveValsAssembler;

  public IValueObjRTValidationAssembler getBeforeSaveValsAssembler() {
    return beforeSaveValsAssembler;
  }

  public void setBeforeSaveValsAssembler(
      IValueObjRTValidationAssembler beforeSaveValsAssembler) {
    this.beforeSaveValsAssembler = beforeSaveValsAssembler;
  }

  private IValueObjRTDtmAssembler afterModifyDtmsAssembler;

  public IValueObjRTDtmAssembler getAfterModifyDtmsAssembler() {
    return afterModifyDtmsAssembler;
  }

  public void setAfterModifyDtmsAssembler(
      IValueObjRTDtmAssembler afterModifyDtmsAssembler) {
    this.afterModifyDtmsAssembler = afterModifyDtmsAssembler;
  }

  private IValueObjRTDtmAssembler beforeSaveDtmAssembler;

  public IValueObjRTDtmAssembler getBeforeSaveDtmAssembler() {
    return beforeSaveDtmAssembler;
  }

  public void setBeforeSaveDtmAssembler(
      IValueObjRTDtmAssembler beforeSaveDtmAssembler) {
    this.beforeSaveDtmAssembler = beforeSaveDtmAssembler;
  }
  private IValueObjRTDtmAssembler retrieveDefaultDtmsAssembler;

  public IValueObjRTDtmAssembler getRetrieveDefaultDtmsAssembler() {
    return retrieveDefaultDtmsAssembler;
  }

  public void setRetrieveDefaultDtmsAssembler(
      IValueObjRTDtmAssembler retrieveDefaultDtmsAssembler) {
    this.retrieveDefaultDtmsAssembler = retrieveDefaultDtmsAssembler;
  }
}
