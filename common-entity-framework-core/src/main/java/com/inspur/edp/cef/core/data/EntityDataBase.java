/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.core.data.extendhandler.CefDataExtHandler;
import com.inspur.edp.cef.core.data.extendhandler.EntityDataExtHandler;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import java.util.ArrayList;
import java.util.HashMap;

import com.inspur.edp.cef.entity.entity.IValuesContainer;
import lombok.var;

public abstract class EntityDataBase extends CefDataBase implements IEntityData, IValuesContainer {

  @Override
  public final HashMap<String, IEntityDataCollection> getChilds() {
    var childs = innerGetChilds();
    var extedChilds = getExtHandler().getChilds();
    if(extedChilds != null) {
      childs.putAll(extedChilds);
    }
    return childs;
  }

  protected abstract HashMap<String, IEntityDataCollection> innerGetChilds();

  @Override
  public final ICefData createChild(String childCode) {
    RefObject<ICefData> result = new RefObject(null);
    if(getExtHandler().createChild(childCode, result)){
      return result.argvalue;
    }
    return innerCreateChild(childCode);
  }

  protected ICefData innerCreateChild(String childCode){
    throw new RuntimeException();
  }

  @Override
  public final void mergeChildData(String nodeCode, ArrayList<IEntityData> childData) {
    if(getExtHandler().mergeChildData(nodeCode, childData)){
      return;
    }
    innerMergeChildData(nodeCode, childData);
  }

  protected void innerMergeChildData(String nodeCode, ArrayList<IEntityData> childData){
    throw new RuntimeException();
  }

  //region ext

  @Override
  protected CefDataExtHandler createExtHandler() {
    return new EntityDataExtHandler();
  }

  @Override
  protected EntityDataExtHandler getExtHandler() {
    return (EntityDataExtHandler)super.getExtHandler();
  }
  //endregion

  @Override
  public final Object createValue(String propName){
    RefObject<Object> result = new RefObject<>(null);
    if(getExtHandler().createValue(propName, result)){
      return result.argvalue;
    }
    return innerCreateValue(propName);
  }

  protected Object innerCreateValue(String propName){
    return null;
  }
}
