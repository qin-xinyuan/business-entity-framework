/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors;

import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.action.CompReflector;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.event.query.CefQueryEventBroker;

import java.util.Objects;

public abstract class BaseDtmAdaptor implements IDetermination {

  private final Class type;
  private final String name;
  private final CompReflector reflector;

  public BaseDtmAdaptor(String name, Class type, CompReflector reflector) {
    this.name = name;
    this.type = type;
    this.reflector = reflector;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void execute(ICefDeterminationContext ctx, IChangeDetail change) {
    Objects.requireNonNull(reflector, "reflector");
    AbstractDeterminationAction dtm = (AbstractDeterminationAction) reflector
        .createInstance(ctx, change);
    CefQueryEventBroker.fireProcessRecordClassPath(dtm);
    dtm.Do();
  }
}
