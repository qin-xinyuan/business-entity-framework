/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package  com.inspur.edp.cef.core.determination;

import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

//用于AfterModify/BeforeSave 依赖于变更集
//TODO: BE原来有个DeterminationContext和ValidationContext
//RuntimeDeterminationAssembler主表一个每个子表一个
public class EntityDeterminationExecutor
{

		///#region Ctor
	private IEntityRTDtmAssembler privateAssembler;
	protected final IEntityRTDtmAssembler getAssembler()
	{
		return privateAssembler;
	}

	private ICefEntityContext privateEntityContext;
	protected final ICefEntityContext getEntityContext()
	{
		return privateEntityContext;
	}

	public EntityDeterminationExecutor(IEntityRTDtmAssembler assembler, ICefEntityContext entityCtx)
	{
		DataValidator.checkForNullReference(assembler, "assembler");
		DataValidator.checkForNullReference(entityCtx, "entityCtx");

		privateAssembler = assembler;
		privateEntityContext = entityCtx;
	}

	private IChangeDetail privateChange;
	protected final IChangeDetail getChange()
	{
		return privateChange;
	}
	protected final void setChange(IChangeDetail value)
	{
		privateChange = value;
	}
	private ICefDeterminationContext privateContext;
	public final ICefDeterminationContext getContext()
	{
		return privateContext;
	}
	public final void setContext(ICefDeterminationContext value)
	{
		privateContext = value;
	}
	public void execute()
	{
		setContext(GetContext());
		setChange(getChangeset());
		if ((getChange() == null))
		{
			return;
		}
		executeBelongingDeterminations();
		executeDeterminations();
		executeChildDeterminations();
	}

	protected final void executeBelongingDeterminations()
	{
		java.util.List<IDetermination> dtms = getAssembler().getBelongingDeterminations();
		if (dtms == null || dtms.isEmpty())
		{
			return;
		}
		for (IDetermination dtm : dtms)
		{
			executeDtmCore(dtm);
		}
	}

	protected final void executeDeterminations()
	{
		java.util.List<IDetermination> dtms = getAssembler().getDeterminations();
		if (dtms == null || dtms.isEmpty())
		{
			return;
		}
		for (IDetermination dtm : dtms)
		{
			executeDtmCore(dtm);
		}
	}

	protected final void executeChildDeterminations()
	{
		java.util.List<IDetermination> dtms = getAssembler().getChildAssemblers();
		if (dtms == null || dtms.isEmpty())
		{
			return;
		}

		for (IDetermination dtm : dtms)
		{
			executeDtmCore(dtm);
		}
	}

	protected void executeDtmCore(IDetermination dtm)
	{
		if (!dtm.canExecute(getChange()))
		{
			return;
		}
		dtm.execute(getContext(), getChange());
	}

	protected ICefDeterminationContext GetContext()
	{
		return getAssembler().getDeterminationContext(getEntityContext());
	}

	protected IChangeDetail getChangeset()
	{
		IChangeDetail changeDetail=getAssembler().getChangeset(getEntityContext());
		if(changeDetail==null)
			return null;
		return changeDetail.clone();
	}
}
