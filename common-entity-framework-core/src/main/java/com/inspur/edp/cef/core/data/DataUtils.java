/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.inspur.edp.cef.api.dataType.base.IAccessorCreator;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.accessor.dynamicProp.DynamicPropSetAccessor;
import com.inspur.edp.cef.entity.accessor.dynamicProp.DynamicPropSetReadonlyAccessor;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropSet;
import com.inspur.edp.cef.spi.common.UdtManagerUtil;
import com.inspur.edp.cef.spi.entity.IAuthFieldValue;
import com.inspur.edp.udt.entity.IUdtData;
import java.math.BigDecimal;
import java.util.Date;

public class DataUtils {

  public  static boolean isStringEquals(String value1,String value2)
  {
    if(value1==null||value1.equals(""))
      return value2==null||value2.equals("");
    return value1.equals(value2);
  }

  public static boolean isIntEquals(Integer value1,Integer value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }

  public static boolean isBooleanEquals(boolean value1,boolean value2)
  {
    return value1==value2;
  }

  public static boolean isBooleanEquals(Boolean value1,Boolean value2) {
      return value1==null ? value2==null : value1.equals(value2);
  }

  public static  boolean isDecimalEquals(BigDecimal value1,BigDecimal value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }

  public  static boolean isAssoValueEqueals(IAuthFieldValue value1,IAuthFieldValue value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }

  public  static boolean isUdtEquals(IUdtData value1,IUdtData value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }

  public  static boolean isBytesEqueals(byte[] value1,byte[] value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }

  public static  boolean isDateEqueals(Date value1,Date value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }
  public  static boolean isEnumEqueals(Enum value1,Enum value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }

  public  static  boolean isDynamicEqueals(IDynamicPropSet  value1,IDynamicPropSet value2)
  {
    if(value1==null)
      return value2==null;
    return value1.equals(value2);
  }


  //region util

  public static <T extends IValueObjData> T createAccNested(IAccessor acc, String configId,
      String propName, boolean isReadonly) {
    if(acc.getInnerData() == null) {
      return null;
    }
    IAccessorCreator accCreator = UdtManagerUtil.getUdtFactory().createManager(configId)
        .getAccessorCreator();
    IValueObjData data = (IValueObjData) acc.getInnerData().getValue(propName);
    ValueObjAccessor rez = (ValueObjAccessor) (isReadonly ? accCreator.createReadonlyAccessor(data)
        : accCreator.createAccessor(data));
    rez.setBelongBuffer(acc);
    rez.setPropName(propName);
    return (T)rez;
  }

  public static IDynamicPropSet createDynPropSetAcc(IAccessor acc, String propName, Boolean isReadonly) {
    if(acc.getInnerData() == null) {
      return null;
    }
    IDynamicPropSet data = (IDynamicPropSet) acc.getInnerData().getValue(propName);
    com.inspur.edp.cef.entity.accessor.dataType.ValueObjAccessor rez =
        isReadonly ? new DynamicPropSetReadonlyAccessor(data) : new DynamicPropSetAccessor(data);
    rez.setBelongBuffer(acc);
    rez.setPropName(propName);
    return (IDynamicPropSet) rez;
  }
  //endregion
}
