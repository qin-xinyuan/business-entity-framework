/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.scope;

import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;
import com.inspur.edp.cef.api.scope.ScopeExtensionConfig;
import com.inspur.edp.cef.spi.scope.AbstractCefScopeExtension;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import io.iec.edp.caf.runtime.config.CefBeanUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.BeansException;

public class CefScopeExtensionManager {

  private CefScopeExtensionManager() {
    buildConfig();
  }

  private static final String configFileName = "cef_scope.json";
  private static final String configSectionName = "CefScopeConfig";

  private Map<String, ScopeExtensionConfig> configDict;

  static {
    privateInstance = new CefScopeExtensionManager();
  }

  private void buildConfig() {
    configDict = new HashMap<>(7);

    configDict.put("PlatformCommon_Bef_SaveScopeNodeParameter",
        new ScopeExtensionConfig("PlatformCommon_Bef_SaveScopeNodeParameter",
            "com.inspur.edp.bef.core.action.save.SaveWithScopeExtension"));

    configDict.put("PlatformCommon_Bef_RetrieveScopeNodeParameter",
        new ScopeExtensionConfig("PlatformCommon_Bef_RetrieveScopeNodeParameter",
            "com.inspur.edp.bef.core.action.retrieve.RetrieveExtension"));

    configDict.put("PlatformCommon_Bef_CheckVersionScopeNodeParameter",
        new ScopeExtensionConfig("PlatformCommon_Bef_CheckVersionScopeNodeParameter",
            "com.inspur.edp.bef.core.action.retrieve.CheckVersionExtension"));

    configDict.put("PlatformCommon_Bef_AddLockScopeNodeParameter",
        new ScopeExtensionConfig("PlatformCommon_Bef_AddLockScopeNodeParameter",
            "com.inspur.edp.bef.core.action.retrieve.AddLockExtension"));

    configDict.put("PlatformCommon_Bef_FKConstraint",
        new ScopeExtensionConfig("PlatformCommon_Bef_FKConstraint",
            "com.inspur.edp.bef.builtincomponents.FKConstraints.FKConstraintValScopeExtension"));

    configDict.put("PlatformCommon_Bef_UQConstraintValNodeParameter",
        new ScopeExtensionConfig("PlatformCommon_Bef_UQConstraintValNodeParameter",
            "com.inspur.edp.bef.builtincomponents.UQConstraints.QUConstraintValScopeExtension"));

    configDict.put("PlatformCommon_Bef_InSuiteCodeRuleParameter",
            new ScopeExtensionConfig("PlatformCommon_Bef_InSuiteCodeRuleParameter",
                    "com.inspur.edp.bef.insuite.core.determination.InSuiteCodeRuleScopeExtension"));
  }

  private static CefScopeExtensionManager privateInstance;

  public static CefScopeExtensionManager getInstance() {
    return privateInstance;
  }

  @Deprecated
  public final AbstractCefScopeExtension getScopeExtension(String paraType,
      ArrayList<ICefScopeNodeParameter> listPara) throws BeansException{
    return getScopeExtension(paraType, (List<ICefScopeNodeParameter>)listPara);
  }

  public final AbstractCefScopeExtension getScopeExtension(String paraType,
      List<ICefScopeNodeParameter> listPara) throws BeansException {
    ScopeExtensionConfig extConfig = configDict.get(paraType);
    AbstractCefScopeExtension extension = null;
    extension = extConfig != null ?
        (AbstractCefScopeExtension) extConfig.getExtInstance()
        :  SpringBeanUtils.getBean(paraType, AbstractCefScopeExtension.class);
    extension.setParameters(listPara);
    return extension;
//
//    if (configDict.containsKey(paraType) == false) {
//      throw new RuntimeException("\"找不到ScopeNodePara对应的extension:\" " + paraType);
//    }
////			throw new GSPException(ErrorCodes.Scope_ExtensionConfigNotFound, "找不到ScopeNodePara对应的extension:" + paraType);
//    ScopeExtensionConfig extensionConfig = configDict.get(paraType);
//    if (extensionConfig == null || extensionConfig.getClassName() == null
//        || extensionConfig.getClassName() == "") {
//      throw new RuntimeException("{paraType}的CefScope配置不完整");
//    }
////		if (  DotNetToJavaStringHelper.isNullOrEmpty(extensionConfig.Assembly) || DotNetToJavaStringHelper.isNullOrEmpty(extensionConfig.Class))
////		{
////			throw new GSPException(ErrorCodes.Scope_IncompleteConfig, $"{paraType}的CefScope配置不完整");
////		}
//
//    Class c = Class.forName(extensionConfig.getClassName());
//    Constructor constructors = c.getConstructor(null);
//    AbstractCefScopeExtension extensionInst = (AbstractCefScopeExtension) constructors
//        .newInstance(null);
//    if (extensionInst == null) {
//      throw new RuntimeException("{paraType}的CefScope配置的类型不存在");
//    }
//    extensionInst.setParameters(listPara);
//    return extensionInst;
//		{
//			throw new GSPException(ErrorCodes.Scope_InvalidExtensionType, $"{paraType}的CefScope配置的类型必须继承Inspur.Gsp.Cef.Spi.scope.AbstractCefScopeExtension");
//		}
//		var assembly = Assembly.Load(extensionConfig.Assembly);

//		if(type == null)
//		{
//			throw new GSPException(ErrorCodes.Scope_ExtensionTypeNotFound, $"{paraType}的CefScope配置的类型不存在");
//		}
//		Object tempVar = Activator.createInstance(type);

//		if(extensionInst == null)
//		{
//			throw new GSPException(ErrorCodes.Scope_InvalidExtensionType, $"{paraType}的CefScope配置的类型必须继承Inspur.Gsp.Cef.Spi.scope.AbstractCefScopeExtension");
//		}
//		extensionInst.Parameters = listPara;
//		return extensionInst;
  }
}
