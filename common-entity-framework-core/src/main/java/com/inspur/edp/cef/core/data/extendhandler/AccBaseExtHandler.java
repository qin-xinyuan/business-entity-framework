/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data.extendhandler;

import com.inspur.edp.cef.entity.accessor.dataType.ValueObjAccessor;
import com.inspur.edp.cef.entity.accessor.base.AccessorBase;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.accessor.base.OnPropertyChangingListener;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.spi.extend.datatype.ICefDataExtend;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.var;

//TODO: 不应该继承CefDataExtHandler
public abstract class AccBaseExtHandler extends CefDataExtHandler implements
    OnPropertyChangingListener {

  private IAccessor belong;

  protected IAccessor getBelong() {
    return belong;
  }

  private final boolean isReadonly;

  protected boolean getIsReadonly() {
    return isReadonly;
  }

  public AccBaseExtHandler(IAccessor belong, boolean isReadonly) {
    Objects.requireNonNull(belong, "belong");

    this.belong = belong;
    this.isReadonly = isReadonly;
  }

  @Override
  protected ICefData createData(ICefDataExtend ext) {
    return getIsReadonly() ? ext.createReadonlyAccessor(getBelong())
        : ext.createAccessor(getBelong());
  }

  @Override
  public void OnPropertyChanging(IAccessor changingAcc, String propName, Object value,
      Object orgValue, IAccessor rootAcc) {
    ((AccessorBase) getBelong()).firePropertyChanging(getBelong(), propName, value, orgValue);
  }

  @Override
  protected void onAddingExtend(ICefDataExtend ext, ICefData extendData) {
    ((IAccessor) extendData).addPropertyChangingListener(this);
  }

  public void acceptChange(IChangeDetail change) {
    Objects.requireNonNull(change, "change");

    List<ICefData> extendDatas = getExtendDatas();
    if (extendDatas == null) {
      return;
    }
      for (ICefData data : extendDatas) {
        if (data instanceof IAccessor) {
          ((IAccessor) data).acceptChange(change);
        }
      }
  }

  public void onInnerDataChanged() {
    List<ICefData> extendDatas = getExtendDatas();
    if (extendDatas == null) {
      return;
    }
    for (ICefData data : extendDatas) {
      if (data instanceof IAccessor) {
        ((IAccessor) data).setInnerData(getBelong().getInnerData());
      }
    }
  }

  public AccBaseExtHandler copy(IAccessor belong) throws CloneNotSupportedException {
    AccBaseExtHandler rez = (AccBaseExtHandler) clone();
    rez.belong = belong;
    if (extendList != null) {
      rez.extendList = new ArrayList<>(extendList);
      rez.extendDatas = extendDatas.stream().map(item -> item.copy())
          .collect(Collectors.toList());
      for (int i = 0; i < extendList.size(); i++) {
        rez.onAddingExtend(rez.extendList.get(i), rez.extendDatas.get(i));
      }
    }
    return rez;
  }
}
