/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.buffer.base;

import com.inspur.edp.cef.api.buffer.ICefBuffer;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;

public class CefBuffer implements ICefBuffer
{
	private IAccessor _data;

	public IAccessor getData()
	{
		return _data;
	}
	public final void setData(IAccessor value)
	{
		_data = value;
		if (_data != null)
		{
			setIsReadonly(_data.getIsReadonly());
		}
	}

	private boolean privateIsReadonly;
	public final boolean getIsReadonly()
	{
		return privateIsReadonly;
	}
	private void setIsReadonly(boolean value)
	{
		privateIsReadonly = value;
	}

	public CefBuffer(IAccessor inner)
	{
		setData(inner);
	}
}
