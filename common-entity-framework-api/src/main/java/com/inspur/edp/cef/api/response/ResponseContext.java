/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.response;

import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageCollector;
import com.inspur.edp.cef.entity.changeset.ChangeDetailMerger;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class ResponseContext {

  // region 内部变更
  private java.util.HashMap<String, IChangeDetail> innerChangeset;

  public java.util.HashMap<String, IChangeDetail> getInnerChangeset() {
    return (innerChangeset != null)
        ? innerChangeset
        : (innerChangeset = new java.util.HashMap<String, IChangeDetail>());
  }

  public final void mergeInnerChange(IChangeDetail... details) {
    // DataValidator.CheckForNullReference(details, "details");

    for (IChangeDetail detail : details) {
      IChangeDetail existing = null;
      existing = getInnerChangeset().get(detail.getDataID());
      if (existing == null) {
        getInnerChangeset().put(detail.getDataID(), detail);
        return;
      }
      // 新增变更集合并一个修改变更集在ChangeDetailMerger中是异常情况, 但是在这里是正常的.
      if (existing.getChangeType() == ChangeType.Added && detail.getChangeType() == ChangeType.Modify) {
        return;
      }
      ChangeDetailMerger.mergeChangeDetail(detail, existing);
    }
  }

  public void clearInnerChange() {
    getInnerChangeset().clear();
  }

  private IChangeDetail privateVariableInnerChange;

  public final IChangeDetail getVariableInnerChange() {
    return privateVariableInnerChange;
  }

  public final void setVariableInnerChange(IChangeDetail value) {
    privateVariableInnerChange = value;
  }

  // endregion

  // region 消息
  public MessageCollector messageCollector = new MessageCollector();

  public java.util.List<IBizMessage> getMessages() {
    return this.messageCollector.getMessages();
  }

  public final void addMessage(IBizMessage... msgList) {
    // DataValidator.CheckForNullReference(msgList, "msgList");

    // C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
    for (IBizMessage msg : msgList) {
      this.messageCollector.addMessage(msg);
    }
  }

  public final void addMessage(java.util.List<IBizMessage> msgList) {
    // DataValidator.CheckForNullReference(msgList, "msgList");

    addMessage(msgList.toArray(new IBizMessage[] {}));
  }

  // C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by
  // C# to Java Converter:
  public void clearMessage() {
    this.messageCollector.clear();
  }
  // endregion
}
