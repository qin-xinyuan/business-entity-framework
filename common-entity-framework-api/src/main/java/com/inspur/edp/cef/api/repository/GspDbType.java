/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//TODO：J版临时添加，实际属于Inspur.Ecp.Caf.DataAccess
package com.inspur.edp.cef.api.repository;

//
// 摘要:
//     数据库类型
public enum GspDbType {
	//
	// 摘要:
	//     SqlServer类型
	SQLServer,//0
	//
	// 摘要:
	//     Oracle类型
	Oracle,//1,
	//
	// 摘要:
	//     PostgreSQL类型
	PgSQL, //2,
	//
	// 摘要:
	//     MySQL类型
	MySQL,//3,
	//
	// 摘要:
	//     DM类型
	DM,//4,
	//
	//摘要：
	//     HighGo类型
	HighGo,//5
	//
	//摘要：
	//     GBase类型
	GBase,//6

	Oscar,

	//人大金仓
	Kingbase,


	// DB2类型
	DB2,
	//
	// 摘要:
	//     未知类型
	Unknown,// =255
}
