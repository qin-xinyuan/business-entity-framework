/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.response;

import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class ResponseInfo
{
	private Object privateReturnValue;
	public final Object getReturnValue()
	{
		return privateReturnValue;
	}
	public final void setReturnValue(Object value)
	{
		privateReturnValue = value;
	}
	private java.util.HashMap<String, IChangeDetail> privateInnerDataChange;
	public final java.util.HashMap<String, IChangeDetail> getInnerDataChange()
	{
		return privateInnerDataChange;
	}
	public final void setInnerDataChange(java.util.HashMap<String, IChangeDetail> value)
	{
		privateInnerDataChange = value;
	}
	private java.util.List<IBizMessage> privateMessage;
	public final java.util.List<IBizMessage> getMessage()
	{
		return privateMessage;
	}
	public final void setMessage(java.util.List<IBizMessage> value)
	{
		privateMessage = value;
	}
	private IChangeDetail privateInnerVariableChange;
	public final IChangeDetail getInnerVariableChange()
	{
		return privateInnerVariableChange;
	}
	public final void setInnerVariableChange(IChangeDetail value)
	{
		privateInnerVariableChange = value;
	}

}
