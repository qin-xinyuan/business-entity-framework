/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.authority;

public class AuthorityInfo
{
	/** 
	 权限字段名称
	 
	*/
	private String privateFieldName;
	public final String getFieldName()
	{
		return privateFieldName;
	}
	public final void setFieldName(String value)
	{
		privateFieldName = value;
	}

	/** 
	 权限关联字段
	 
	*/
	private String privateSourceFieldName;
	public final String getSourceFieldName()
	{
		return privateSourceFieldName;
	}
	public final void setSourceFieldName(String value)
	{
		privateSourceFieldName = value;
	}
	/** 
	 权限sql片段
	 
	*/
	private String privateAuthoritySql;
	public final String getAuthoritySql()
	{
		return privateAuthoritySql;
	}
	public final void setAuthoritySql(String value)
	{
		privateAuthoritySql = value;
	}
}
