/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.dataType.entity;

import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;

public interface ICefEntityContext extends ICefDataTypeContext {


  @Override
  IEntityData getData();


  void setData(IEntityData value);

  //TODO: 这个地方传给业务代码的时候里面显然不能带Determinate和validate方法,
  //ICefDataType接口给业务代码的和内部用的得拆开;

  @Override
  ICefEntity getDataType();


  void setDataType(ICefEntity value);

  //IChangesetManager ChangesetManager { get; set; }

  IChangeDetail getChange();

  void setChange(IChangeDetail value);

  //BE merely
  //IChangeDetail TransactionChange { get; }

  //IBufferManager BufferManager { get; set; }
}
