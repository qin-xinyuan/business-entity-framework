/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo.repoproccessor;

import com.inspur.edp.cef.api.repository.readerWriter.ICefReader;
import com.inspur.edp.cef.repository.adaptoritem.BaseAdaptorItem;
import com.inspur.edp.cef.repository.adaptoritem.dbprocessor.DbProcessor;
import com.inspur.edp.cef.repository.dbcolumninfo.DbColumnInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.EnumPropertyInfo;

public class EnumRepoDbProcessor extends AbstractRepoPrcessor {

  @Override
  public Object readProperty(DbColumnInfo dbColumnInfo, DbProcessor dbProcessor,
      BaseAdaptorItem adaptorItem, ICefReader reader) {
    EnumPropertyInfo enumPropertyInfo = (EnumPropertyInfo) dbColumnInfo.getDataTypePropertyInfo()
        .getObjectInfo();
    Object obj = reader.readValue(dbColumnInfo.getColumnName());
    try {
      return enumPropertyInfo.getEnumValueFromDac(obj);
    }
    catch (Exception e)
    {
      String objValue = obj == null ? "null" : obj.toString();
      throw new RuntimeException("将枚举字段" + dbColumnInfo.getDataTypePropertyInfo().getPropertyName() +
          "的数据库值转换为枚举类型时出错，数据库值为：" + objValue + "，错误消息：" + e.getMessage(), e);
    }
  }

  @Override
  public Object getPropertyChangeValue(DbColumnInfo dbColumnInfo, Object value) {
    return value;
  }

  @Override
  public Object getPersistenceValue(DbColumnInfo dbColumnInfo, Object value) {
    return dbColumnInfo.getTypeTransProcesser().transType(value);
  }
}
