//package Inspur.Gsp.Cef.Repository.datacache;
//
//import Inspur.Gsp.Cef.Entity.*;
//
//public class EntityDataCacheService
//{
//	private static EntityDataCacheService instance;
//	private EntityDataCacheService()
//	{
//	}
//
//	public static EntityDataCacheService getInstance()
//	{
//		if (instance == null)
//		{
//			instance = new EntityDataCacheService();
//		}
//		return instance;
//	}
//
//	public final boolean HasCacheData(String dataID, String cacheConfigID)
//	{
//		throw new NotImplementedException();
//	}
//
//	public final void AddDataCacheLock(String dataID, String cacheConfigID)
//	{
//	}
//
//	public final void ReleaseDataCacheLock(String dataID, String cacheConfigID)
//	{
//	}
//
//	public final java.util.ArrayList<IEntityData> getDatas(String cacheConfigID, java.util.List<String> dataIDs, RefObject<java.util.ArrayList<String>> missingDataIDs)
//	{
//		missingDataIDs.argvalue = new java.util.ArrayList<String>();
//		java.util.ArrayList<IEntityData> list = new java.util.ArrayList<IEntityData>();

//		{
//			IEntityData data = GetData(cacheConfigID, item);
//			if (data == null)
//			{
//				missingDataIDs.argvalue.add(item);
//			}
//			list.add(data);
//		}
//		return list;
//	}
//
//	public final IEntityData GetData(String cacheConfigID, String dataID)
//	{
//		AddDataCacheLock(dataID, cacheConfigID);
//		try
//		{
//			return EntityMemoryCacheAdapter.getInstance().GetData(cacheConfigID, dataID);
//		}
//		finally
//		{
//			ReleaseDataCacheLock(dataID, cacheConfigID);
//		}
//	}
//
//	public final void AddDatasToCache(String cacheConfigID, java.util.List<IEntityData> datas)
//	{
//		if (datas == null || datas.isEmpty())
//		{
//			return;
//		}

//		{
//			AddDataToCache(cacheConfigID, item);
//		}
//	}
//
//	public final void AddDataToCache(String cacheConfigID, IEntityData data)
//	{
//		AddDataCacheLock(data.getID(), cacheConfigID);
//		try
//		{
//			ICefData tempVar = data.Copy();
//			EntityMemoryCacheAdapter.getInstance().SetDataToCache(cacheConfigID, (IEntityData)((tempVar instanceof IEntityData) ? tempVar : null));
//		}
//		finally
//		{
//			ReleaseDataCacheLock(data.getID(), cacheConfigID);
//		}
//	}
//
//	public final void RemoveDataFromCache(String cacheConfigID, String dataID)
//	{
//		AddDataCacheLock(dataID, cacheConfigID);
//		try
//		{
//			EntityMemoryCacheAdapter.getInstance().RemoveCache(cacheConfigID, dataID);
//		}
//		finally
//		{
//			ReleaseDataCacheLock(dataID, cacheConfigID);
//		}
//	}
//}