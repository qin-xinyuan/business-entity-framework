/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;
import com.inspur.edp.cef.spi.entity.info.EnumValueInfo;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.EnumPropertyInfo;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;

public class Enum2IntProcesser implements ITypeTransProcesser {

    public static Enum2IntProcesser getInstacne(Class enumType,EnumPropertyInfo enumPropertyInfo)
    {
       return new Enum2IntProcesser(enumType,enumPropertyInfo);
    }

    private java.lang.Class privateEnumType;
    private final EnumPropertyInfo enumPropertyInfo;

    public final java.lang.Class getEnumType() {
        return privateEnumType;
    }

    public final void setEnumType(java.lang.Class value) {
        privateEnumType = value;
    }
    public Enum2IntProcesser(java.lang.Class enumType) {
        this.setEnumType(enumType);
        this.enumPropertyInfo=null;
    }
    public Enum2IntProcesser(java.lang.Class enumType,EnumPropertyInfo enumPropertyInfo) {
        this.setEnumType(enumType);
        this.enumPropertyInfo=enumPropertyInfo;
    }

    public final Object transType(FilterCondition filter, Connection db) {
        filter.setValue(GetEnumIndex(filter.getValue()));
        return transType(filter.getValue());
    }

    public final Object transType(Object value) {
        //后续校验不符合格式
        if (value == null) {
            return null;
        } else if (value instanceof Integer)
            return value;
        else if (value instanceof String)
        {
            if(isExistStringIndex(privateEnumType,value.toString()))
            {
                return value.toString();
            }
            else {
                return Integer.parseInt((String) value);
            }

        }
        else if (value.getClass() == privateEnumType) {
            try {
                Method method = value.getClass().getMethod("getValue");
                return method.invoke(value);

            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(value.toString() + "枚举转换失败", e);
            }
        }
        throw new RuntimeException("输入的枚举类型");
    }

    @Override
    public Object transType(Object value, boolean isNull) {
        return this.transType(value);
    }

    private  String GetEnumIndex(Object value) {
        if (value == null) {
            return null;
        }
        if(privateEnumType!=null) {
            try {
                if (isExistStringIndex(privateEnumType, value.toString())) {
                    try {
                        Class cls = Class.forName(privateEnumType.getName());
                        try {
                            Object object = Enum.valueOf(cls, value.toString());
                            Method method = object.getClass().getMethod("getValue");
                            String strTemp = method.invoke(object).toString();
                            return strTemp;
                        } catch (Exception e1) {
                            return value.toString();
                        }
                    } catch (Exception ex) {
                        throw new RuntimeException("获取枚举值失败！枚举值为："
                                + value.toString() + ",枚举类为："
                                + privateEnumType.getName());
                    }
                } else {
                    int result = Integer.parseInt(value.toString());
                    return (new Integer(result)).toString();
                }
            } catch (RuntimeException e) {

                try {
                    if (value.toString().isEmpty()) {
                        return null;
                    }
                    Class cls = Class.forName(privateEnumType.getName());
                    Object object = Enum.valueOf(cls, value.toString());
                    Method method = object.getClass().getMethod("getValue");
                    String strTemp = method.invoke(object).toString();
                    return strTemp;
                } catch (Exception ex) {
                    throw new RuntimeException("获取枚举值失败！枚举值为："
                            + value.toString() + ",枚举类为："
                            + privateEnumType.getName(), ex);
                }
            }
        }
        else
            {
                EnumValueInfo enumValueInfo= enumPropertyInfo.getEnumValueInfo(value.toString());
                if(enumValueInfo==null)
                {
                    int result = Integer.parseInt(value.toString());
                    return (new Integer(result)).toString();
                }
                else
                    return enumValueInfo.getIndex().toString();
            }
    }
    public  boolean isExistStringIndex(Class enumtype,String value)  {

        try {

            Class cla=Class.forName(enumtype.getName());
            Method method= cla.getMethod("isSringIndexType");
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
