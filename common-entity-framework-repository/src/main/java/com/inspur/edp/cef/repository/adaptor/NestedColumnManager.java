/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import com.inspur.edp.cef.api.repository.INestedPersistenceValueReader;
import com.inspur.edp.cef.repository.assembler.nesteddatatype.NestedColumnInfo;

import java.util.HashMap;

public class NestedColumnManager {
    private static NestedColumnManager instance = new NestedColumnManager();
    private java.util.HashMap<String, NestedColumnInfo> columns = new java.util.HashMap<String, NestedColumnInfo>();


    private HashMap<String, Boolean> initMap = new HashMap<String, Boolean>();
    private HashMap<String, HashMap<String, NestedColumnInfo>> nestedColumns = new HashMap();
    private final Object lockObj = new Object();
    private NestedColumnManager(){

    }

    public HashMap<String, Boolean> getInitMap() {
        return initMap;
    }

    public void setInitMap(HashMap<String, Boolean> initMap) {
        this.initMap = initMap;
    }

    public static NestedColumnManager getInstance(){
        return instance;
    }

    public HashMap<String, NestedColumnInfo> getColumns(String className){
        if(!nestedColumns.containsKey(className)){
            synchronized (lockObj){
                if(!nestedColumns.containsKey(className)){
                    nestedColumns.put(className, new HashMap<>());
                }
            }
        }
        return nestedColumns.get(className);
    }

    protected void addColumnInfo(String className, String columnName, INestedPersistenceValueReader persistenceValueReader, boolean isMultiLang, boolean isUdt, boolean isAssociation, boolean isEnum, String belongElementLabel) {
        if(!getColumns(className).containsKey(columnName)){
            synchronized (getColumns(className)){
                if(!getColumns(className).containsKey(columnName)){
                    NestedColumnInfo tempVar = new NestedColumnInfo();
                    tempVar.setColumnName(columnName);
                    tempVar.setBelongElementLabel(belongElementLabel);
                    tempVar.setIsAssociation(isAssociation);
                    tempVar.setIsEnum(isEnum);
                    tempVar.setIsAssociateRefElement(false);
                    tempVar.setIsMultiLang(isMultiLang);
                    tempVar.setIsUdtElement(isUdt);
                    tempVar.setPersistenceValueReader(persistenceValueReader);
                    getColumns(className).put(columnName, tempVar);
                }
            }
        }
    }
}
