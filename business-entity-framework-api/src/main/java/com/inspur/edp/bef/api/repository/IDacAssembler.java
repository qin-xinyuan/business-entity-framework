/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.repository;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.sql.Connection;
import java.sql.ResultSet;

public interface IDacAssembler
{

		//region 关联

	String getTableNameByColumns(java.util.HashMap<String, String> columns, String keyColumnName, RefObject<String> keyDbColumnName, java.util.ArrayList<String> tableAlias);

	//void InitTableAlias(List<string> tableAlias);

		//endregion


		//region SQL

		//region 工具
	Object buildParam(Connection db, String paramName, java.sql.Types dataType, int size, Object paramValue);
	Object buildParam(Connection db, String paramName, java.sql.Types dataType, Object paramValue);
	String trans2DbColumn(String filedName);
	java.sql.Types getDataType(String columnName);


		//endregion
	String getPrimaryKey();
	String getTableNameWithoutAlias();
	String getAlias();



		//region Query
	String getQueryTableName();
	String getQueryColumns();


		//region 排序

	String GetDefaultSort();


		//endregion


		//region 过滤

	String GetDefaultCondition(Connection db, java.util.ArrayList<Object> parameter);


		//endregion



		//endregion


		//region Insert

	String GetInsertColumnNames();
	String GetInsertValueParamter();

	Object[] BuildInsertParamters(Connection db, IEntityData entityData);


		//endregion


		//region Modify
	Object GetPropertyChangeValue(String propertyName, Object propertyValue);

	Object GetPropertyChangeValueWithTrans(String propertyName, Object propertyValue);

		//endregion

		//endregion


		//region 工具相关

	String GetTableInfo();

	ITypeTransProcesser GetTypeTransProcesser(String fieldName);

		//endregion


		//region 实体导入

	IEntityData ImportData(ResultSet dr);


		//endregion


		//region 子组装器
	java.util.HashMap<String, IChildDacAssembler> GetChildAssembler();

	String getNodeCode();

		//endregion

}
