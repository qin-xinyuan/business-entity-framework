/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.be;

import com.inspur.edp.bef.api.action.IBefCallContext;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.variable.api.data.IVariableData;

/** BE子节点的上下文信息接口 */
public interface IBENodeEntityContext extends ICefEntityContext {
  /** */
  IBENodeEntityContext getParentContext();

  /** 获取当前实体数据的唯一标识  */
  String getID();

  /** BE编号 */
  String getCode();

  /** 获取当前请求下因修改触发了联动计算而使实体数据发生变化产生的变更集  */
  IChangeDetail getCurrentTemplateChange();

  /**
   * 当前请求下还未提交内存的实体数据变更集，CurrentChange可认为是ChangeFromModify与CurrentTemplateChange的合集
   */
  IChangeDetail getCurrentChange();

  /** 获取子表当前实体数据
   * @see IBEContext#getCurrentData()
   */
  IEntityData getCurrentData();

  /** 获取内存中已提交成功的实体数据变更集  */
  IChangeDetail getTransactionalChange();

  IChangeDetail getCurrentTransactionalChange();

  /** 获取子表事务级实体数据
   *
   * @see IBEContext#getTransactionData()
   */
  IEntityData getTransactionData();

  /** 获取子表原始实体数据
   *
   * @see IBEContext#getOriginalData()
   */
  IEntityData getOriginalData();

  /**
   * 根据当前Session创建Lcp
   *
   * @param config
   * @return Lcp
   */
  IStandardLcp getLcp(String config);

  /** 获取变量, 只读 */
  	IVariableData getVariables();

  /** 增加创建批量处理参数 */
  void addScopeParameter(ICefScopeNodeParameter parameter);

  /** 保存后是否清空缓存，默认不清空。设置此属性将影响当前session下所有此be类型的数据。 */
  BufferClearingType getBufferClearingType();

  void setBufferClearingType(BufferClearingType value);

  //region i18n
  ModelResInfo getModelResInfo();
  /**
   * 翻译后的实体名称
   *
   * @return
   */
  String getEntityI18nName();
  /**
   * 翻译后的属性名称
   *
   * @param labelID
   * @return 翻译后的属性名称
   */
  String getPropertyI18nName(String labelID);
  /**
   * 翻译后的关联带出字段名称
   *
   * @param labelID
   * @param refLabelID
   * @return
   */
  String getRefPropertyI18nName(String labelID, String refLabelID);
  /**
   * 翻译后的枚举显示值
   *
   * @param labelID
   * @param enumKey
   * @return
   */
  String getEnumValueI18nDisplayName(String labelID, String enumKey);
  /**
   * 翻译后的唯一性约束提示信息
   *
   * @param conCode
   * @return
   */
  String getUniqueConstraintMessage(String conCode);

  /**
   * 获取一个存储区对象
   * 详见{@link IBefCallContext}
   * @return 存储区对象
   *
   * @see IBefCallContext
   */
  IBefCallContext getCallContext();
  //endregion
}
