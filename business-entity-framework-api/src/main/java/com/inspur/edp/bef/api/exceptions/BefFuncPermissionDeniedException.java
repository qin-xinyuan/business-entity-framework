/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;

import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;

/** BefManager动作执行时无操作权限异常 */
public class BefFuncPermissionDeniedException extends BefExceptionBase {
  // private const string exceptionCode = "";
  // private const string resourceFile = "";
  private static final String msg = "您无权执行此操作"; // TODO: 临时,将来改成资源文件

  /** 构造函数 */
  public BefFuncPermissionDeniedException() {
    super(
        ErrorCodes.FuncPermissionDenied,
        msg,
        null,
        ExceptionLevel.Warning,
        true);
  }
}
