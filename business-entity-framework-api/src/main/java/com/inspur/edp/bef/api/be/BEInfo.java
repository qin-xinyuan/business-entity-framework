/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.be;

public class BEInfo  {
	private String beType;
	private String beId;
	private String rootEntityCode;

	public String getBEType() {
		return beType;
	}

	public void setBEType(String beType) {
		this.beType = beType;
	}

	public String getBEID() {
		return beId;
	}

	public void setBEID(String beId) {
		this.beId = beId;
	}

	public String getRootEntityCode() {
		return rootEntityCode;
	}

	public void setRootEntityCode(String rootEntityCode) {
		this.rootEntityCode = rootEntityCode;
	}

	@Override
	public Object clone()  {
		BEInfo info =new BEInfo();
		info.beId=beId;
		info.beType=beType;
		info.rootEntityCode=rootEntityCode;
		return info;
	}
}
