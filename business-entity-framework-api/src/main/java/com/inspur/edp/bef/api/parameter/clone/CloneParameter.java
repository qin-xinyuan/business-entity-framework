/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.parameter.clone;

import java.util.HashMap;
import java.util.List;

public class CloneParameter {

    /**
     * 要复制的节点集合
     */
    private List<String> includeNodes;
    /**
     * 要排除的节点集合
     */
    private List<String> excludeNodes;

    /**
     * 节点上要复制的字段集合
     */
    private HashMap<String, List<String>> includes;
    /**
     * 节点上要排除的字段集合
     */
    private HashMap<String, List<String>> excludes;

    public HashMap<String, List<String>> getIncludes() {
        return includes;
    }

    public HashMap<String, List<String>> getExcludes() {
        return excludes;
    }

    public HashMap<String, List<String>> getInclude() {
        return includes;
    }

    public List<String> getIncludeNodes() {
        return includeNodes;
    }

    public void setIncludeNodes(List<String> includeNodes) {
        this.includeNodes = includeNodes;
    }

    public List<String> getExcludeNodes() {
        return excludeNodes;
    }

    public void setExcludeNodes(List<String> excludeNodes) {
        this.excludeNodes = excludeNodes;
    }

    public void setIncludeProps(String nodeCode, List<String> props){
        if(includes == null){
            includes = new HashMap<>();
        }
        includes.put(nodeCode, props);
    }

    public void setExcludeProps(String nodeCode, List<String> props){
        if(excludes == null){
            excludes = new HashMap<>();
        }
        excludes.put(nodeCode, props);
    }

}
