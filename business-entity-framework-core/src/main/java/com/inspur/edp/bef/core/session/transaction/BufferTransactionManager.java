/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.transaction;

import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.FuncSession.SessionItemMap;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.cef.api.session.ICefSessionItem;
import java.util.Map;

public class BufferTransactionManager {

  private final FuncSession session;

  // region 构造函数
  public BufferTransactionManager(FuncSession session) {
    this.session = session;
  }

  @Deprecated
  public static BufferTransactionManager getInstance() {
    return new BufferTransactionManager(FuncSessionManager.getCurrentSession());
  }
  // endregion

  private int trans;

  public final void Begin() {
    trans++;
    SessionItemMap currItems = (SessionItemMap) session.getSessionItemsStack().peek();
    SessionItemMap newTranItems = session.newSessionItemMap();

    for (Map.Entry<String, ICefSessionItem> currItem : currItems.entrySet()) {
      if (currItem.getValue() instanceof IBefTransactionItem) {
        newTranItems.innerPut(currItem.getKey(),
            (ICefSessionItem) ((IBefTransactionItem) currItem.getValue()).begin());
      } else {
        newTranItems.innerPut(currItem.getKey(), currItem.getValue());
      }
    }
    session.getSessionItemsStack().push(newTranItems);
  }

  public final void SetComplete() {
    CheckTransaction();
    Map<String, ICefSessionItem> toCompleteItems = session.getSessionItemsStack().pop();
    SessionItemMap currItems = (SessionItemMap) session.getSessionItemsStack().peek();

    for (Map.Entry<String, ICefSessionItem> toCompleteItem : toCompleteItems.entrySet()) {
      ICefSessionItem currItem = currItems.get(toCompleteItem.getKey());
      if (currItem != null) {
        if (toCompleteItem.getValue() instanceof IBefTransactionItem) {
          ((IBefTransactionItem) currItem).commit((IBefTransactionItem) toCompleteItem.getValue());
        }
      } else {
        currItems.innerPut(toCompleteItem.getKey(), toCompleteItem.getValue());
      }
    }
    trans--;
  }

  /**
   * 回滚事务
   */
  public final void SetAbort() {
    CheckTransaction();
    Map<String, ICefSessionItem> upperItems = session.getSessionItemsStack().pop();
    Map<String, ICefSessionItem> currItems = session.getSessionItemsStack().peek();

    for (Map.Entry<String, ICefSessionItem> currItem : currItems.entrySet()) {
      if (currItem.getValue() instanceof IBefTransactionItem) {
        ICefSessionItem upperItem = upperItems.get(currItem.getKey());
        if (upperItem != null) {
          ((IBefTransactionItem) currItem.getValue()).rollBack((IBefTransactionItem) upperItem);
        }
      }
    }
    trans--;
  }

  // 验证当前存在事务
  private void CheckTransaction() {
    if (trans <= 0) {
      throw new RuntimeException();
    }
  }
}
