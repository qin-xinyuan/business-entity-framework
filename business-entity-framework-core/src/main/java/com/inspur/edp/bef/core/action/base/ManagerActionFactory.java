/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.base;

import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.api.parameter.clone.CloneParameter;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.clear.AutoClearBufferMgrAction;
import com.inspur.edp.bef.core.action.clear.ClearBufferMgrAction;
import com.inspur.edp.bef.core.action.clear.ClearChangesetMgrAction;
import com.inspur.edp.bef.core.action.clear.ClearLockMgrAction;
import com.inspur.edp.bef.core.action.delete.DeleteChildMgrAction;
import com.inspur.edp.bef.core.action.delete.DeleteMgrAction;
import com.inspur.edp.bef.core.action.delete.MultiDeleteMgrAction;
import com.inspur.edp.bef.core.action.modify.ModifyByFilterMgrAction;
import com.inspur.edp.bef.core.action.modify.ModifyMgrAction;
import com.inspur.edp.bef.core.action.modify.MultiModifyMgrAction;
import com.inspur.edp.bef.core.action.query.QueryManagerAction;
import com.inspur.edp.bef.core.action.query.QueryWithBufferMgrAction;
import com.inspur.edp.bef.core.action.query.QueryWithoutAuthInfoMgrAction;
import com.inspur.edp.bef.core.action.retrieve.EditManagerAction;
import com.inspur.edp.bef.core.action.retrieve.IsEffectiveMgrAction;
import com.inspur.edp.bef.core.action.retrieve.MultiEffectiveMgrAction;
import com.inspur.edp.bef.core.action.retrieve.MultiRetrieveMgrAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveChidAndChildMgrAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveChildAndChildAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveChildMgrAction;
import com.inspur.edp.bef.core.action.retrieve.RetrieveManagerAction;
import com.inspur.edp.bef.core.action.retrievedefault.*;
import com.inspur.edp.bef.core.action.save.SaveManagerAction;
import com.inspur.edp.bef.core.action.serialize.EntityDataDeserMgrAction;
import com.inspur.edp.bef.core.action.serialize.EntityDataSerMgrAction;
import com.inspur.edp.bef.core.action.variable.VariableModifyMgrAction;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.List;

public class ManagerActionFactory
{
	public final RetrieveManagerAction getRetrieveManagerAction(BEManagerContext managerContext, String id, RetrieveParam para)
	{
		return new RetrieveManagerAction(managerContext, id, para);
	}

	public final EditManagerAction getEditManagerAction(BEManagerContext managerContext, String id)
	{
		return new EditManagerAction(managerContext, id);
	}

	public final RetrieveChildMgrAction getRetrieveChildManagerAction(BEManagerContext managerContext, java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, RetrieveParam par)
	{
		return new RetrieveChildMgrAction(managerContext, nodeCodes, hierachyIdList, par);
	}

	public final RetrieveChidAndChildMgrAction getRetrieveChildAndChildManagerAction(BEManagerContext managerContext, java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, java.util.ArrayList<String> ids,RetrieveParam par)
	{
		return new RetrieveChidAndChildMgrAction(managerContext, nodeCodes, hierachyIdList,ids, par);
	}

	public final MultiRetrieveMgrAction getMultiRetrieveMgrAction(BEManagerContext managerContext, List<String> idList, RetrieveParam para)
	{
		return new MultiRetrieveMgrAction(managerContext, idList, para);
	}

	public final SaveManagerAction getSaveManagerAction(BEManagerContext managerContext)
	{
		return new SaveManagerAction(managerContext);
	}

//ORIGINAL LINE: internal RetrieveDefaultManagerAction GetRetrieveDefaultManagerAction(BEManagerContext managerContext, bool useID, string dataID, IDictionary<string, object> defaultValues = null)
	public final RetrieveDefaultManagerAction getRetrieveDefaultManagerAction(BEManagerContext managerContext, boolean useID, String dataID, java.util.Map<String, Object> defaultValues)
	{
		return new RetrieveDefaultManagerAction(managerContext, useID, dataID, defaultValues);
	}

	public final CloneMgrAction getCloneMgrAction(BEManagerContext managerContext, String cloneDataId, String dataID, CloneParameter cloneParameter)
	{
		return new CloneMgrAction(managerContext, cloneDataId, dataID, cloneParameter);
	}

	public final MultiRetrieveDefaultMgrAction getMultiRetrieveDefaultMgrAction(BEManagerContext managerContext, List<String> dataIDs, java.util.Map<String, Object> defaultValues)
	{
		return new MultiRetrieveDefaultMgrAction(managerContext, dataIDs, defaultValues);
	}

	public final CreateObjectDataMgrAction getCreateObjectDataMgrAction(IBEManager manager, boolean useID, String dataID)
	{
		return new CreateObjectDataMgrAction(manager, useID, dataID);
	}

	public final CreateChildDataMgrAction getCreateChildDataMgrAction(IBEManager mgr, String childCode, boolean useID, String dataID)
	{
		return new CreateChildDataMgrAction(mgr, childCode, useID, dataID);
	}

	public final RetrieveDefaultChildMgrAction getRetrieveDefaultChildManagerAction(BEManagerContext managerContext,
			java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, String dataID, java.util.Map<String, Object> defaultValues)
	{
		return new RetrieveDefaultChildMgrAction(managerContext, nodeCodes, hierachyIdList, dataID, defaultValues);
	}


	public final MultiRetrieveDefaultChildMgrAction getMultiRetrieveDefaultChildMgrAction(
		BEManagerContext managerContext, java.util.List<String> nodeCodes,
		java.util.List<String> hierachyIdList, List<String> dataIDs) {
		return new MultiRetrieveDefaultChildMgrAction(managerContext, nodeCodes, hierachyIdList,
			dataIDs);
	}

	public final ModifyMgrAction getModifyMgrAction(BEManagerContext managerContext, IChangeDetail changeDetail)
	{
		return new ModifyMgrAction(managerContext, changeDetail);
	}

	public final MultiModifyMgrAction getMultiModifyMgrAction(BEManagerContext managerContext, java.util.ArrayList<IChangeDetail> changeList)
	{
		return new MultiModifyMgrAction(managerContext, changeList);
	}

	public final ModifyByFilterMgrAction getModifyByFilterMgrAction(BEManagerContext managerContext, EntityFilter entityFilter, ModifyChangeDetail changeDetail)
	{
		return new ModifyByFilterMgrAction(managerContext, entityFilter, changeDetail);
	}

	public final QueryManagerAction getQueryMgrAction(BEManagerContext managerContext, String nodeCode, EntityFilter filter)
	{
		return new QueryManagerAction(managerContext, nodeCode, filter);
	}

    public final QueryWithoutAuthInfoMgrAction getQueryWithoutAuthInfoMgrAction(BEManagerContext managerContext, String nodeCode, EntityFilter filter)
    {
        return new QueryWithoutAuthInfoMgrAction(managerContext, nodeCode, filter);
    }
	public final QueryWithBufferMgrAction getQueryWithBufferMgrAction(BEManagerContext managerContext, String nodeCode, EntityFilter filter)
	{
		return new QueryWithBufferMgrAction(managerContext, nodeCode, filter);
	}

	public final DeleteMgrAction getDeleteMgrAction(BEManagerContext managerContext, String dataId)
	{
		return new DeleteMgrAction(managerContext, dataId);
	}

	public final MultiDeleteMgrAction getMultiDeleteMgrAction(BEManagerContext managerContext, java.util.List<String> dataIds)
	{
		return new MultiDeleteMgrAction(managerContext, dataIds);
	}

	public final DeleteChildMgrAction getDeleteChildMgrAction(BEManagerContext managerContext, java.util.List<String> nodeCodes, java.util.List<String> hierachyIdList, java.util.List<String> ids)
	{
		return new DeleteChildMgrAction(managerContext, nodeCodes, hierachyIdList, ids);
	}

	public final EntityDataSerMgrAction getDataSerAction(BEManagerContext managerContext, IEntityData data)
	{
		return new EntityDataSerMgrAction(managerContext, data);
	}

	public final EntityDataSerMgrAction getDataSerAction(BEManagerContext managerContext, IEntityData data,boolean keepAssoPropertyForExpression)
	{
		return new EntityDataSerMgrAction(managerContext, data,keepAssoPropertyForExpression);
	}

	public final EntityDataDeserMgrAction getDataDeSerAction(BEManagerContext mgrContext, String data)
	{
		return new EntityDataDeserMgrAction(mgrContext, data);
	}

	public final ClearChangesetMgrAction getClearChangesetMgrAction(BEManagerContext managerContext)
	{
		return new ClearChangesetMgrAction(managerContext);
	}

	public final ClearLockMgrAction getClearLockMgrAction(BEManagerContext managerContext)
	{
		return new ClearLockMgrAction(managerContext);
	}
	public final ClearBufferMgrAction getClearBufferMgrAction(BEManagerContext managerContext)
	{
		return new ClearBufferMgrAction(managerContext);
	}
	public final AutoClearBufferMgrAction getAutoClearBufferMgrAction(BEManagerContext managerContext)
	{
		return new AutoClearBufferMgrAction(managerContext);
	}
	public final VariableModifyMgrAction getVariableModifyMgrAction(BEManagerContext managerContext, IChangeDetail changeDetail)
	{
		return new VariableModifyMgrAction(managerContext, changeDetail);
	}

	public final IsEffectiveMgrAction getIsEffectiveMgrAction(BEManagerContext managerContext, String nodeCode, String dataId)
	{
		return new IsEffectiveMgrAction(managerContext, nodeCode, dataId);
	}

	public final MultiEffectiveMgrAction getMultiEffectiveMgrAction(BEManagerContext mgrContext,String nodeCode,List<String> dataIDs)
	{
		return new MultiEffectiveMgrAction(mgrContext,nodeCode,dataIDs);
	}

	public final MultiEffectiveMgrAction getMultiEffectiveMgrAction(BEManagerContext mgrContext,String nodeCode,List<String> dataIDs, String propName)
	{
		return new MultiEffectiveMgrAction(mgrContext,nodeCode,dataIDs, propName);
	}
}
