/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.action.determination.IBeforeRetrieveDtmContext;
import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.LcpUtil;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.api.message.MessageLevel;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;
import com.inspur.edp.cef.variable.api.data.IVariableData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BeforeRetrieveDtmContext implements IDeterminationContext, IBeforeRetrieveDtmContext {
    private IBEManagerContext mgrContext;
    private List<String> dataIds;
    private RetrieveParam retrieveParam;

    @Deprecated
    public BeforeRetrieveDtmContext(IBEManagerContext mgrContext,List<String> dataIds)
    {
        this(mgrContext, dataIds, new RetrieveParam());
    }

    public BeforeRetrieveDtmContext(IBEManagerContext mgrContext,List<String> dataIds, RetrieveParam retrieveParam)
    {
        this.mgrContext = mgrContext;
        this.dataIds = dataIds;
        this.retrieveParam = retrieveParam;
    }

    @Override
    public RetrieveParam getRetrieveParam() {
        return retrieveParam;
    }

    private Map<String, EntityFilter> nodeFilters=new HashMap<>();
    @Override
    public Map<String, EntityFilter> getNodeFilters() {
        return nodeFilters;
    }

    @Override
    public List<String> getDataIds() {
        return dataIds;
    }

    @Override
    public IBENodeEntity getBENodeEntity() {
        throw new RuntimeException();
    }

    @Override
    public IBENodeEntityContext getBEContext() {
        throw new RuntimeException();
    }

    @Override
    public IChangeDetail getRootChange() {
        throw new RuntimeException();
    }

    @Override
    public String getNodeCode() {
        throw new RuntimeException();
    }

    @Override
    public void setNodeCode(String value) {
        throw new RuntimeException();
    }

    @Override
    public IEntityData getCurrentEntityData() {
        throw new RuntimeException();
    }

    @Override
    public ICefData getData() {
        throw new RuntimeException();
    }

    @Override
    public void addMessage(IBizMessage msg) {
        throw new RuntimeException();
    }

    @Override
    public IBizMessage createMessageWithLocation(MessageLevel level, String msg, String... msgPars) {
        throw new RuntimeException();
    }

    @Override
    public final IBizMessage createMessageWithLocation(
            MessageLevel level, java.util.List<String> columnNames, String msg, String... msgPars) {
        throw new RuntimeException();
    }

    @Override
    public IEntityData getCurrentData() {
        throw new RuntimeException();
    }

    @Override
    public IStandardLcp getLcp(String configId) {
        return LcpUtil.getLcp(configId);
    }

    @Override
    public IVariableData getVariables() {
        return  ((BEManagerContext)mgrContext).getVarBufferManager().getReadonlyCurrentData();
    }

    @Override
    public String getConfigId() {
        return mgrContext.getBEManager().getBEType();
    }

    @Override
    public IEntityData getOriginalData() {
        throw new RuntimeException();
    }

    @Override
    public IEntityData getTransactionData() {
        throw new RuntimeException();
    }
    // endregion

    // region i18n
    @Override
    public ModelResInfo getModelResInfo() {
        return mgrContext.getModelResInfo();
    }

    public final String getEntityI18nName() {
        throw new RuntimeException("QueryDeterminationContext中未指定实体");
    }

    public final String getPropertyI18nName(String labelId) {
        throw new RuntimeException("QueryDeterminationContext中未指定实体");
    }

    public final String getRefPropertyI18nName(String labelId, String reflabelId) {
        throw new RuntimeException("QueryDeterminationContext中未指定实体");
    }

    public final String getEnumValueI18nDisplayName(String labelId, String enumKey) {
        throw new RuntimeException("QueryDeterminationContext中未指定实体");
    }

    @Override
    public final String getUniqueConstraintMessage(String conCode) {
        throw new RuntimeException("QueryDeterminationContext中未指定实体");
    }

    @Override
    public final String getEntityI18nName(String nodeCode) {
        return mgrContext.getEntityI18nName(nodeCode);
    }

    @Override
    public final String getPropertyI18nName(String nodeCode, String labelId) {
        return mgrContext.getPropertyI18nName(nodeCode, labelId);
    }

    @Override
    public final String getRefPropertyI18nName(String nodeCode, String labelId, String reflabelId) {
        return mgrContext.getRefPropertyI18nName(nodeCode, labelId, reflabelId);
    }

    @Override
    public final String getEnumValueI18nDisplayName(String nodeCode, String labelId, String enumKey) {
        return mgrContext.getEnumValueI18nDisplayName(nodeCode, labelId, enumKey);
    }

    @Override
    public final String getUniqueConstraintMessage(String nodeCode, String conCode) {
        return mgrContext.getUniqueConstraintMessage(nodeCode, conCode);
    }
}
