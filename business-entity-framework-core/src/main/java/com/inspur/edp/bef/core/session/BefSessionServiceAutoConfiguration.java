/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.api.repository.IBefRepositoryFactory;
import com.inspur.edp.bef.api.services.IBefSessionManager;
import io.iec.edp.caf.core.context.BizContextBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class BefSessionServiceAutoConfiguration {
  @Bean
  public IBefSessionManager getBefSessionManager() {
    return new BefSessionManager();
  }
@Bean
  public BizContextBuilder getBefBizContextBuilder(){return new BefBizContextBuilder();}
}
