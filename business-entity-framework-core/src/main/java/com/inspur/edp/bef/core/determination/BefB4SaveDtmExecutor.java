/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

public class BefB4SaveDtmExecutor extends EntityDeterminationExecutor {

  private IChangeDetail change;

  // region Ctor
  public BefB4SaveDtmExecutor(
      IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx, IChangeDetail change) {
    super(assembler, entityCtx);
    this.change = change;
  }
  // endregion

  // region Execute
  protected IBENodeEntityContext getBEContext() {
    return (IBENodeEntityContext) super.getEntityContext();
  }

  @Override
  public void execute() {
    if (isRoot()) {
      ((CoreBEContext)getBEContext()).acceptTempChange();
      getRootContext().getDataSavePar().clear();
    }

    setChange(getChangeset());
    if (getChange() == null) {
      return;
    }

    BeforeSaveDtmContext dtmCtx = (BeforeSaveDtmContext) GetContext();
    setContext(dtmCtx);

    executeBelongingDeterminations();
    executeDeterminations();

    if (dtmCtx.getConditions() != null && !dtmCtx.getConditions().isEmpty()) {
      getRootContext().getDataSavePar()
          .setFilterCondition(getBEContext().getCode(),
              getBEContext().getID(), dtmCtx.getConditions());
    }

    executeChildDeterminations();

    getRootContext().mergeIntoInnerChange();
    getRootContext().acceptListenerChange();

  }

  private CoreBEContext rootContext;

  public final CoreBEContext getRootContext() {
    if (rootContext == null) {
      IBENodeEntityContext current = getBEContext();
      while (((CoreBEContext) ((current instanceof CoreBEContext) ? current : null)) == null
          || current == null) {
        current = current.getParentContext();
      }
      rootContext = (CoreBEContext) current;
    }
    return rootContext;
  }

  private boolean isRoot() {
    return getBEContext() instanceof CoreBEContext;
  }

  @Override
  protected IChangeDetail getChangeset() {
    return change != null ? change.clone() : null;
  }

  @Override
  protected ICefDeterminationContext GetContext() {
    return new BeforeSaveDtmContext((IBENodeEntityContext) getEntityContext());
  }
  // endregion
}
