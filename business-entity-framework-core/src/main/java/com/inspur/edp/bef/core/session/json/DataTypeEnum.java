/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.json;

public enum  DataTypeEnum
{
    None("z"),
    CharType("0"),
    ByteType("1"),
    IntegerType("2"),
    BoolType("3"),
    FloatType("4"),
    DoubleType("5"),
    ShortType("6"),
    LongType("7"),
    StringType("8"),
    ObjType("a");

    private String value;
    private static java.util.HashMap<String, DataTypeEnum> mappings;
    private synchronized static java.util.HashMap<String, DataTypeEnum> getMappings()
    {
        if (mappings == null)
        {
            mappings = new java.util.HashMap<String, DataTypeEnum>();
        }
        return mappings;
    }

    private DataTypeEnum(String value){
        this.value = value;
        getMappings().put(value, this);
    }

    public String getValue()
    {
        return value;
    }

    public static DataTypeEnum forValue(String value)
    {
        return getMappings().get(value);
    }
}
