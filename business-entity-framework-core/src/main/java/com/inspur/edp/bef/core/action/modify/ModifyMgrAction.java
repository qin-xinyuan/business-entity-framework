/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.modify;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionStack;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.changeset.AddOrModifyChangeDetail;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class ModifyMgrAction extends AbstractManagerAction<VoidActionResult> {

  private IChangeDetail changeDetail;

  public ModifyMgrAction(IBEManagerContext managerContext, IChangeDetail changeDetail) {
    super(managerContext);
    ActionUtil.requireNonNull(changeDetail, "changeDetail");
    this.changeDetail = changeDetail;
  }

  @Override
  public final void execute() {
    getBEManagerContext().checkAuthority("Modify");
//    AuthorityUtil.checkAuthority("Modify");
//    FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Modify");
    IBusinessEntity be = getBEManagerContext().getEntity(changeDetail.getDataID());
    RetrieveParam tempVar = new RetrieveParam();
    tempVar.setNeedLock(true);
    be.retrieve(tempVar);
    if (!be.getBEContext().hasData() && changeDetail instanceof AddOrModifyChangeDetail) {
      be.retrieveDefault();
      changeDetail = ((AddOrModifyChangeDetail) changeDetail).getModifyChange();
    } else {
      checkRetrieveResult(be);
    }
    // com.inspur.edp.bef.core.be.BEContext.ChangeFromModify = changeDetail;
    if (!ActionStack.isLastNode()) {
      ((BusinessEntity) be).getBEContext().getResponse().mergeInnerChange(changeDetail);
    }
    be.modify(changeDetail);
  }

  private void checkRetrieveResult(IBusinessEntity be) {
    if (be.getBEContext().getCurrentData() == null) {
      throw new BefExceptionBase(
          ErrorCodes.ModifyingNonExistence,
          String.format("修改时，ID为[%1$s]的数据不存在，请检查ID是否正确或数据是否已被删除", be.getID()),
          null, ExceptionLevel.Error, true);
    }
    LockUtils.checkLocked(be.getBEContext());
  }

  @Override
  protected final IMgrActionAssembler getMgrAssembler() {
    return getMgrActionAssemblerFactory()
        .getModifyMgrActionAssembler(getBEManagerContext(), changeDetail);
  }

//  public static void demandFuncPermission(IBEManagerContext beMgrCtx) {
//    IDefaultMgrActionAssFactory factory =
//        (IDefaultMgrActionAssFactory)
//            ((beMgrCtx.getMgrActionAssemblerFactory() instanceof IDefaultMgrActionAssFactory)
//                ? beMgrCtx.getMgrActionAssemblerFactory()
//                : null);
//    if (factory == null) {
//      return;
//    }
//    ModifyMgrActionAssembler assembler = factory.getModifyMgrActionAssembler(beMgrCtx, null);
//    if (assembler == null) {
//      return;
//    }
//    assembler.demandFuncPermission();
//  }
}
