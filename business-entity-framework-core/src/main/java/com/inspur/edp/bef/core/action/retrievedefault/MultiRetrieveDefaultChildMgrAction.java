/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.exceptions.BefException;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.util.StringUtils;

public class MultiRetrieveDefaultChildMgrAction extends
    AbstractManagerAction<Map<String, IEntityData>> {

   //region 字段属性

   private List<String> nodeCodes;
   private List<String> hierachyIdList;
   private List<String> dataIds;

   //endregion

   //region Consturctor

   public MultiRetrieveDefaultChildMgrAction(IBEManagerContext managerContext,
       List<String> nodeCodes, List<String> hierachyIdList, List<String> dataIds) {
      ActionUtil.requireNonNull(nodeCodes, "nodeCodes");
      ActionUtil.requireNonNull(hierachyIdList, "hierachyIdList");
      ActionUtil.requireNonNull(dataIds, "dataIds");

      this.nodeCodes = nodeCodes;
      this.hierachyIdList = hierachyIdList;
      this.dataIds = dataIds;
   }

   //endregion

   //region execute

   @Override
   public final void execute() {
      getBEManagerContext().checkAuthority("Modify");
//      AuthorityUtil.checkAuthority("Modify");
//      FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Modify");
      IBusinessEntity be = getBEManagerContext().getEntity(hierachyIdList.get(0));

      RetrieveParam tempVar = new RetrieveParam();
      tempVar.setNeedLock(true);
      if (be.retrieve(tempVar).getData() == null) {
         throw new BefException(ErrorCodes.DeletingNonExistence,
             "新增子时主表数据不存在" + hierachyIdList.get(0), null, ExceptionLevel.Error);
      }
      LockUtils.checkLocked(be.getBEContext());
      Map<String, IEntityData> result = new HashMap<>(dataIds.size(), 1);
      for (String id : dataIds) {
         if (StringUtils.isEmpty(id)) {
            throw new RuntimeException("批量新增dataIDs中不允许存在空串");
         }
         IEntityData curr = be.retrieveDefaultChild(nodeCodes, hierachyIdList, id);
         if (result.put(id, curr) != null) {
            throw new RuntimeException("批量新增子表时存在重复的Id:" + id);
         }
      }
      setResult(result);
   }

   //TODO:
   @Override
   protected final IMgrActionAssembler getMgrAssembler() {
      return getMgrActionAssemblerFactory()
          .getRetrieveDefaultChildMgrActionAssembler(getBEManagerContext(), nodeCodes,
              hierachyIdList);
   }
   //endregion
}
