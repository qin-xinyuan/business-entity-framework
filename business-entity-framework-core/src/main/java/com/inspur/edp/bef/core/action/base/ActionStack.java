/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.base;

import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import java.util.Collections;
import java.util.List;

public final class ActionStack {
  private static InheritableThreadLocal<BefActionStack> stack = new InheritableThreadLocal<BefActionStack>();

  public static void createNewStackNode(String funcSessionID) {
    if (stack.get() == null) {
      return;
    }
    stack.set(new BefActionStack(funcSessionID, stack.get()));
  }

  public static void popPreStackNode() {
    if (stack.get() == null) {
      return;
    }
    stack.set(stack.get().popPreStack());
    if (stack.get() != null && stack.get().count() == 0 && stack.get().hasPreStack() == false) {
      stack.set(null);
    }
  }

  public static void pop() {
    // var stack = FuncSessionManager.getCurrentSession().ActionStack;
    if (stack == null || stack.get() == null) {
      return;
    }
      stack.get().pop();
    if (stack.get().count() == 0) {
      if (stack.get().hasPreStack() == false) {
        stack.remove();
      }
    }
  }

  public static void clear() {
    if(stack != null)
      stack.remove();
  }

//  public static void push(String actionName) {
//    if (stack.get() == null) {
//      stack.set(new BefActionStack(FuncSessionManager.getCurrentFuncSessionId()));
//    }
//    stack.get().push(actionName, null);
//  }

  public static void push(FuncSession session, String actionName, IBEManager beMgr) {
//    FuncSession session = FuncSessionManager.getCurrentSession();
    if (stack.get() == null) {
      stack.set(new BefActionStack(session.getSessionId()));
    }
    stack.get().push(actionName, beMgr.getBEType());
    if (!session.getActionMgrList().contains(beMgr)) {
      session.getActionMgrList().add(beMgr);
    }
  }

  public static String getFirstBEType(){
    if (stack.get() == null) {
      throw new RuntimeException();
    }
    return stack.get().getFirstBEType();
  }

  public static List<IBEManager> getActionManagers() {
    throw new RuntimeException();
//    FuncSession currentSession = FuncSessionManager.getCurrentSession();
//    return currentSession != null ? currentSession.getActionMgrList() : Collections.emptyList();
  }

  public static boolean isEmpty() {
    return isOutmost() || stack.get().count() == 0;
  }

  public static boolean isOutmost() {
    return stack == null || stack.get() == null;
  }

  public static boolean isLastNode() {
    return stack != null && stack.get() != null && stack.get().count() == 1;
  }
}
