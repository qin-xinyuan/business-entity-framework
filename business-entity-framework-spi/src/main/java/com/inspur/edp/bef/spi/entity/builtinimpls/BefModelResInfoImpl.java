/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.entity.builtinimpls;

import static com.inspur.edp.bef.api.be.BefLockType.None;

import com.inspur.edp.bef.api.be.BefLockType;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefModelResInfoImpl;

public class BefModelResInfoImpl extends CefModelResInfoImpl {
    public BefModelResInfoImpl(String currentSu, String resMetadataId, String displayKey) {
        super(currentSu, resMetadataId, displayKey);
    }

    public BefModelResInfoImpl(String currentSu, String resMetadataId, String metaDataId, String displayKey) {
        super(currentSu, resMetadataId, metaDataId, displayKey);
    }

    private BefLockType lockType=BefLockType.distributeLock;

    public BefModelResInfoImpl(String currentSu, String resMetadataId, String metaDataId, String displayKey,BefLockType lockType )
    {
        super(currentSu, resMetadataId, metaDataId, displayKey);
        this.lockType=lockType;
    }

    public BefLockType getLockType() {
        return lockType;
    }

    //是否支持在tcc事务中调用
    private boolean tccSupported;

    public boolean getTccSupported() {
        return tccSupported;
    }

    public void setTccSupported(boolean value) {
        this.tccSupported = value;
    }

    //是否自动处理tcc锁
    private boolean autoTccLock = true;

    public boolean getAutoTccLock() {
        return autoTccLock;
    }

    public void setAutoTccLock(boolean value) {
        this.autoTccLock = value;
    }

    //是否自动处理tcc回滚
    private boolean autoCancel = true;

    public boolean getAutoCancel() {
        return autoCancel;
    }

    public void setAutoCancel(boolean value) {
        this.autoCancel = value;
    }

    private boolean autoConfirm = true;

    public boolean getAutoConfirm() {
        return autoConfirm;
    }

    public void setAutoConfirm(boolean value) {
        this.autoConfirm = value;
    }
}
