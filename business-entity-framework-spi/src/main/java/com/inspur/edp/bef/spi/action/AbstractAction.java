/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action;

import com.inspur.edp.bef.api.action.assembler.IAbstractActionAssemblerFactory;
import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.spi.action.assembler.entityAssemblerFactory.IDefaultEntityActionAssFactory;
import com.inspur.edp.cef.api.dataType.base.IDataTypeActionAssembler;
import com.inspur.edp.cef.spi.entityaction.CefDataTypeAction;

/** BE实体自定义动作的抽象基类； TResult指定了自定义动作的返回值类型，需要在实现类中指定为具体类型 */
public abstract class AbstractAction<TResult> extends CefDataTypeAction<TResult> {
  private TResult result;

  public AbstractAction() {}

  public AbstractAction(IBENodeEntityContext context) {
    setContext(context);
  }

  /** 获取该自定义动作的业务实体上下文 */
//  @Override
//  public IBENodeEntityContext getContext() {
//    return (IBENodeEntityContext) super.getContext();
//  }
protected IBENodeEntityContext getBEContext() {return (IBENodeEntityContext)super.getContext();}

  protected void setContext(IBENodeEntityContext value) {
    super.setContext(value);
  }

  // region 组装器工厂
  /**
   * 获取组装器工厂
   *
   * @return 自定义动作组装器工厂
   */
  protected final <TAssemblerFactory extends IAbstractActionAssemblerFactory>
      TAssemblerFactory getAssemblerFactory() {
    return (TAssemblerFactory) ((IBENodeEntity) getContext().getDataType()).getAssemblerFactory();
  }

  protected final IDefaultEntityActionAssFactory GetAssemblerFactory() {
    return getAssemblerFactory();
  }

  protected final IDataTypeActionAssembler _getActionAssembler() {
    return getAssembler();
  }
  // endregion

  // region 可重写
  /**
   * 获取BE实体动作组装器，默认情况下获取BEActionAssembler;该方法可复写，可指定扩展的组装器
   *
   * @return BE实体组装器接口
   */
  protected IBEActionAssembler getAssembler() {
    return GetAssemblerFactory().getDefaultAssembler(getBEContext());
  }
  // endregion
}
