/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action.validation;

import com.inspur.edp.bef.api.action.validation.IAfterSaveValidationContext;
import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.cef.entity.repository.DataSaveResult;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public abstract class AbstractAfterSaveValidation extends AbstractValidation {

  protected AbstractAfterSaveValidation(IValidationContext context, IChangeDetail change) {
    super(context, null);
  }

  @Override
  public IAfterSaveValidationContext getContext() {
    return (IAfterSaveValidationContext) super.getContext();
  }

  protected DataSaveResult getDataSaveResult() {
    return getContext().getSaveResult();
  }
}
