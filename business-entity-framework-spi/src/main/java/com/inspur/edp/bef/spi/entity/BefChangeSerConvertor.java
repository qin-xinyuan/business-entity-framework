/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.entity;

import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import java.util.ArrayList;

public class BefChangeSerConvertor extends AbstractBizEntityChangeJsonSerializer {
    private final String code;
    private final boolean isRoot;
    private final String type;
    private final CefEntityResInfoImpl info;

    public BefChangeSerConvertor(String objectCode, boolean isRoot, String beType, CefEntityResInfoImpl resourceInfo){
        super(objectCode, isRoot, beType, getTypes(resourceInfo));
        this.code = objectCode;
        this.isRoot = isRoot;
        this.type = beType;
        this.info = resourceInfo;
    }
    private static java.util.ArrayList<AbstractEntitySerializerItem> getTypes(CefEntityResInfoImpl resourceInfo){
        java.util.ArrayList<AbstractEntitySerializerItem> list=new ArrayList<>();
        list.add(new com.inspur.edp.cef.spi.jsonser.builtinimpls.CefEntityDataSerializerItem(resourceInfo));
        return list;
    }
    @Override
    protected com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeSerializer getChildEntityConvertor(  java.lang.String nodeCode){
        if (info.getChildEntityResInfos() != null && BeMapIgnoreKeysUtil.containsIgnoreKey(info.getChildEntityResInfos().keySet(), nodeCode)) {
            return new BefChangeSerConvertor(BeMapIgnoreKeysUtil.getRelKey(info.getChildEntityResInfos().keySet(), nodeCode), false, type, (CefEntityResInfoImpl) BeMapIgnoreKeysUtil.getValueByIgnoreKey(info.getChildEntityResInfos(), nodeCode));
        }
        return null;
    }
    @Override
    protected com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerializer getCefDataConvertor(){
        return new BefEntitySerConvertor(code, isRoot, type, info);
    }
}
