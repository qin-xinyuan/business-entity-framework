/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.util;

import java.util.List;

public class ArrayUtil {
	public static <T> boolean isArrayEquals(List<T> array1, List<T> array2) {
		if (array1 == null) {
			if (array2 == null) {
				return true;
			} else {
				return false;
			}
		}
		if (array2 == null) {
			return false;
		}
		if (array1.size() != array2.size()) {
			return false;
		}
		for (int i = 0; i < array1.size(); i++) {
			if (array1.get(i).equals(array2.get(i))) {
				return false;
			}
		}
		return true;
	}
}
