/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.entity;

public abstract class AssoInfoBase implements IValuesContainer {

    //region getValue
    @Override
    public final Object getValue(String propName) {
        return innerGetValue(propName);
    }
    protected Object innerGetValue(String propName){
        return null;
    }
    //endregion

    //region setValue
    @Override
    public final void setValue(String propName, Object value) {
        innerSetValue(propName, value);
    }

    protected void innerSetValue(String propName, Object value){

    }
    //endregion

    //region createValue
    @Override
    public final Object createValue(String propName) {
        return innerCreateValue(propName);
    }

    protected Object innerCreateValue(String propName) {
        return null;
    }

    protected boolean equals(String value1,String value2)
    {
        if(value1==null||value1.equals(""))
        {
            if(value2==null||value2.equals(""))
                return true;
            return false;
        }
        return value1.equals(value2);
    }

    public abstract String getValue();

    @Override
    public boolean equals(Object obj) {
        if(obj == null || !(obj instanceof AssoInfoBase)){
            return false;
        }
        return equals(getValue(), ((AssoInfoBase)obj).getValue());
    }
}
