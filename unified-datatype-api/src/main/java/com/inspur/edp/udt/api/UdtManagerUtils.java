/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.api;

import io.iec.edp.caf.runtime.config.CefBeanUtil;

public final class UdtManagerUtils  {

    private static IUdtRepositoryFactory udtRepositoryFactory;
    public static IUdtRepositoryFactory getUdtRepositoryFactory()
    {
        if(udtRepositoryFactory == null)
            udtRepositoryFactory = CefBeanUtil.getAppCtx().getBean(IUdtRepositoryFactory.class);
        return udtRepositoryFactory;
    }

    private static IUdtFactory udtFactory;
    public static IUdtFactory getUdtFactory()
    {
        if(udtFactory == null)
            udtFactory = CefBeanUtil.getAppCtx().getBean(IUdtFactory.class);
        return udtFactory;
    }
}
