/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.Determination;

import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.udt.api.Determination.IDeterminationContext;
import java.util.Objects;

public class TplFieldRtrimDtmDecorator implements IDetermination {

  private IDetermination inner;
  public TplFieldRtrimDtmDecorator(com.inspur.edp.cef.spi.determination.IDetermination fieldRtrimDtm){
    Objects.requireNonNull(fieldRtrimDtm,"fieldRtrimDtm");
    this.inner = fieldRtrimDtm;
  }


  @Override
  public String getName() {
    return null;
  }

  @Override
  public boolean canExecute(IChangeDetail change) {
    return inner.canExecute(change);
  }

  @Override
  public void execute(ICefDeterminationContext context,
      IChangeDetail change) {
//      if(!((IDeterminationContext)context).getTemplateValues().getFieldRtrim()){
//        return;
//      }
    inner.execute(context, change);
  }
}
