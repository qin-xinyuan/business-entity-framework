//package com.inspur.edp.cef.variable.core.determination.builtinimpls;
//
//import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
//import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObjContext;
//import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
//import com.inspur.edp.cef.core.determination.builtinimpls.ValueObjRetriveDefaultDtmAssembler;
//import com.inspur.edp.cef.entity.changeset.IChangeDetail;
//import com.inspur.edp.cef.variable.api.variable.IVariableContext;
//import com.inspur.edp.cef.variable.core.determination.VarDeterminationContext;
//
//public class VarAfterCreateDtmAssembler extends ValueObjRetriveDefaultDtmAssembler {
//
//  @Override
//  public ICefDeterminationContext getDeterminationContext(ICefValueObjContext iCefValueObjContext) {
//    return new VarDeterminationContext(iCefValueObjContext);
//  }
//
//  @Override
//  public IChangeDetail getChangeset(ICefDataTypeContext iCefDataTypeContext) {
//    return ((IVariableContext)iCefDataTypeContext).getInnerChange();
//  }
//}
