package com.inspur.edp.cef.variable.core.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.api.manager.valueObj.ICefValueObjManagerContext;
import com.inspur.edp.cef.api.repository.INestedRepository;
import com.inspur.edp.cef.core.manager.CefValueObjManager;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.variable.api.data.IVariableData;
import com.inspur.edp.cef.variable.api.manager.IVariableManager;
import com.inspur.edp.cef.variable.api.variable.IVariableContext;
import com.inspur.edp.cef.variable.core.variable.AbstractVariable;
import com.inspur.edp.cef.variable.core.variable.VariableContext;

public abstract class AbstractVariableManager extends CefValueObjManager implements IVariableManager
{
	@Override
	protected ICefValueObjManagerContext createMgrContext()
	{
		return new VarMgrContext();
	}

	@Override
	public String serialize(ICefData data)
	{
		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();
//		module.addSerializer(converter);
		mapper.registerModule(module);
		try {
			return (mapper.writeValueAsString(data));
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}

	}
	@Override
	public ICefData deserialize(String content)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public final ICefValueObject createValueObject(IValueObjData data)
	{
		if (!(data instanceof IVariableData))
		{
			throw new UnsupportedOperationException("data");
		}

		VariableContext tempVar = new VariableContext();
		tempVar.setData(data);
		return createVariable(tempVar);
	}

	public abstract AbstractVariable createVariable(IVariableContext ctx);


	@Override
	public INestedRepository getRepository() {
		return null;
	}
}