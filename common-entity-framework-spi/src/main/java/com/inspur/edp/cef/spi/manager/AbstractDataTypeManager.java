/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.manager;

import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.spi.mgraction.CefMgrAction;
import com.inspur.edp.cef.api.manager.ICefManagerContext;

public abstract class AbstractDataTypeManager
{
	private ICefManagerContext privateContext;
	public final ICefManagerContext getContext()
	{
		return privateContext;
	}
	public final void setContext(ICefManagerContext value)
	{
		privateContext = value;
	}


		///#region Action
	protected <TResult> TResult execute(CefMgrAction<TResult> action)
	{
		DataValidator.checkForNullReference(action, "action");

		MgrActionExecutor<TResult> tempVar = new MgrActionExecutor<TResult>();
		tempVar.setAction(action);
		tempVar.setContext(getContext());
		return tempVar.execute();
	}

		///#endregion
}
