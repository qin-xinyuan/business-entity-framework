/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls;

import com.inspur.edp.cef.api.CefRtBeanUtil;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.ModelResInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public abstract class CefModelResInfoImpl extends ModelResInfo {

  private final String currentSu;
  private final String resMetadataId;
  private final String metaDataId;
  private final String displayKey;
  private EntityResInfo rootEntityResInfo;
  private Map<String, EntityResInfo> entityResInfoMap;


  public CefModelResInfoImpl(String currentSu, String resMetadataId, String displayKey) {
    this(currentSu, resMetadataId, "", displayKey);
  }

  public CefModelResInfoImpl(String currentSu, String resMetadataId, String metaDataId, String displayKey) {
    this.currentSu = currentSu;
    this.resMetadataId = resMetadataId;
    this.metaDataId = metaDataId;
    this.displayKey = displayKey;
  }

  @Override
  public final EntityResInfo getCustomResource(String objCode) {
    String lowerCode = objCode.toLowerCase();
    EntityResInfo resInfo = getEntityResInfos().get(lowerCode);
    if (resInfo != null) {
      return resInfo;
    }

    throw new RuntimeException("无效节点编号：" + objCode);
  }

  protected void addCustomResource(CefEntityResInfoImpl entityResInfo) {
    getEntityResInfos().put(entityResInfo.getEntityCode().toLowerCase(), entityResInfo);
  }

  protected final void setRootEntityResInfo(EntityResInfo root) {
    rootEntityResInfo = root;
  }

  /**
   * 模型名称
   *
   * @return
   */
  @Override
  public String getModelDispalyName() {
    return getResourceItemValue(displayKey);
  }

  /**
   * 主节点编号
   */
  @Override
  public String getRootNodeCode() {
    return rootEntityResInfo.getEntityCode();
  }

  /**
   * 资源元数据ID
   */
  @Override
  public String getResourceMetaId() {
    return resMetadataId;
  }

  public String getMetaDataId() {
    return metaDataId;
  }
  public String getConfigId(){
    return "";
  }
  public final String getResourceItemValue(String resourceKey) {
    String resValue = "";
    try{
      resValue = CefRtBeanUtil.getResourceItemValue(currentSu, resMetadataId, resourceKey);
    }
    catch (Exception ex){
      throw new RuntimeException("获取资源值报错:su=["+currentSu+"] beid=["+metaDataId+"] resMetaDataId=["+resMetadataId+"] resourceKey=["+resourceKey+"]", ex);
    }
    return resValue;
  }

  public Map<String, EntityResInfo> getEntityResInfos() {
    if (entityResInfoMap == null && rootEntityResInfo != null) {
      HashMap<String, EntityResInfo> entityMaps = new HashMap<>();
      Stack<EntityResInfo> stack = new Stack<>();
      stack.push(rootEntityResInfo);
      do {
        EntityResInfo info = stack.pop();
        entityMaps.put(info.getEntityCode().toLowerCase(), info);
        if(info.getChildEntityResInfos() != null) {
          info.getChildEntityResInfos().values().stream().forEach(item -> stack.push(item));
        }
      } while (!stack.isEmpty());
      entityResInfoMap = entityMaps;
    }
    return entityResInfoMap;
  }
}
